***Test Cases***
Check that intro is dramatic
    Given the secret agent is in a bar
    When someone asks his name
    Then he presents himself
    and drinks cocktail



***Keywords***
the secret agent is in a bar
    No Operation

someone asks his name
    log     Who are you

he presents himself
    log     Bond
    log     James Bond

drinks cocktail
    log     Cocktail good
