
***Keywords***
Add two numbers
    [Arguments]     ${firstnumber}      ${secondnumber}
    ${result}=      Evaluate    ${firstnumber}+${secondnumber}
    [Return]    ${result}


***Test Cases***
Test return value
    ${r1}=    Add two numbers   10  20
    log     ${r1}


Test add method
    [Template]  Add two numbers
    1   2
    10  20
    -1  5  
    -5  -10
    