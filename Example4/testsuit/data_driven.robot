*** Settings ***
Documentation     A test suite containing tests related to invalid login.
Resource          resource.robot
Suite Teardown    Close Browser
Suite Setup       Create default user  
Test Template     Login With Invalid Credentials Should Fail

*** Test Cases ***               USER NAME        PASSWORD
Invalid Username                 invalid          ${PASSWORD}
Invalid Password                 ${USERNAME}      invalid
Invalid Username And Password    invalid          whatever

*** Keywords ***
Create default user
    Open Browser To URL
    Sign Up Default User
    Go to login page

Login With Invalid Credentials Should Fail
    [Arguments]    ${username}    ${password}
    Input credentials   ${username}     ${password}
    Click Element   //input[@value='Test Login']
    Login failed