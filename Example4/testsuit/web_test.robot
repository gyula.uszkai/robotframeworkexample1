*** Settings ***
Documentation       A test suite for login attempt
Resource            resource.robot
Test Teardown       Close Browser
Test Setup          Open Browser To URL

***Test Cases***

Test login user    
    Sign Up Default User
    Login user      ${USERNAME}     ${PASSWORD}
    Login successful

Test bad login
    Login user      Sanyika     Kerekpar
    Login failed
